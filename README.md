# Final Project - Lyrics Alignment
11020CS573100 Music Information Retrieval Final Project

## About us
- Group 4 黎哥粉絲後援會清大分會
    - 107000216 倪又晞
    - 108000202 石郁琳
    - 108000219 翁玉芯

## Introduction
- Deploy Link: https://108000219.gitlab.io/lyrics-alignment-demo
- This is a simple website for a quick demo of our final project _Lyrics Alignment_
- As it is too simple, we strongly recommend you to browse the website with a 14-inch computer (full screen)
- The website consists of 2 pages, Home Page & Try It Page

## Home Page
- There's a briefly introduction of our works, and a demo video of **Taylor Swift's 22**
- You can try other songs by clicking the button `Try other songs`

## Try It Page
- In this page, you should upload 3 files:
    - mp3 file of the song
    - txt file of the original lyrics of the song
    - txt file of the result of alignment (You should run our colab pipeline first to get it)
- After that, click `play`, and the lyrics will start showing on the screen
- Whenever you pause or restart the audio, you should refresh the page again. 

